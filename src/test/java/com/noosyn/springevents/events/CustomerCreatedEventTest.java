package com.noosyn.springevents.events;

import java.util.UUID;

import com.noosyn.springevents.core.Event;
import com.noosyn.springevents.core.EventMapper;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;

class CustomerCreatedEventTest {
    @Test
    void shouldSerializeFromJsonString() {
        String id = UUID.randomUUID().toString();

        String eventData = String.format("""
        {
            "id" : "%s",
            "occurredOn" : "2021-05-07T23:02:18.673434",
            "payload" : 1
        }
        """, id);

        Event event = EventMapper.mapToObj(eventData, "com.noosyn.springevents.events.CustomerCreated");

        BDDAssertions.assertThat(event).isNotNull();
        BDDAssertions.assertThat(event).isInstanceOf(CustomerCreated.class);
        BDDAssertions.assertThat(event.getOccurredOn()).isNotNull();

    }
}