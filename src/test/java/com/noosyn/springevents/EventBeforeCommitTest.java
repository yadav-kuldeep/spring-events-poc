package com.noosyn.springevents;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(classes = SpringEventsPocApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
class EventBeforeCommitTest {
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Test
    void shouldPublishEventBeforeCommitAndAfterCompletion() {
       String requestBody = """
       {
           "name" : "Kuldeep",
           "email" : "kuldeep.yadav@noosyntech.in"
       }
       """;

       HttpHeaders headers = new HttpHeaders();
       headers.setContentType(MediaType.APPLICATION_JSON);

       ResponseEntity<Void> response = restTemplate.exchange(
           "/customers",
           HttpMethod.POST,
           new HttpEntity<String>(requestBody, headers),
           Void.class
       );

       assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
    
}
