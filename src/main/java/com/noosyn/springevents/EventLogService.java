package com.noosyn.springevents;


import com.noosyn.springevents.models.EventLog;
import com.noosyn.springevents.repository.EventLogRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EventLogService {
    private final EventLogRepository repository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void upsert(EventLog eventLog) {
        repository.save(eventLog);
    }

    public void markDone(String id) {
        System.out.println(id);
        EventLog eventLog = repository.findById(id)
                                    .orElseThrow();
        eventLog.markDone();
        repository.save(eventLog);
    }
    
}
