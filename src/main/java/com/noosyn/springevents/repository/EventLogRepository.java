package com.noosyn.springevents.repository;

import com.noosyn.springevents.models.EventLog;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventLogRepository extends JpaRepository<EventLog, String>{
    
}
