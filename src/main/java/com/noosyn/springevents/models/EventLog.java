package com.noosyn.springevents.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Table(name = "event_log")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EventLog {
    @Id
    @NotNull
    private String id;

    @NotNull
    private String type;

    @NotNull
    @Column(name = "occurred_on")
    private LocalDateTime occuredOn;

    @Column(name = "is_success")
    private boolean isDone = false;

    @Column(columnDefinition = "text")
    @NotNull
    private String data;

    private EventLog(String id, String type, LocalDateTime occurredOn, String data) {
        this.id = id;
        this.type = type;
        this.occuredOn = occurredOn;
        this.data = data;
    }

    public void markDone() {
        this.isDone = !isDone;
    }
    
    public static EventLog of(String id, String type, LocalDateTime occurredOn, String data) {
        return new EventLog(id, type, occurredOn, data);
    }
}
