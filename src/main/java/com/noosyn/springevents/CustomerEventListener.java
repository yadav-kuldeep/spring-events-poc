package com.noosyn.springevents;

import static org.springframework.transaction.event.TransactionPhase.AFTER_COMPLETION;

import com.noosyn.springevents.core.AbstractEventListener;
import com.noosyn.springevents.events.CustomerCreated;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomerEventListener extends AbstractEventListener {
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void onCustomerCreated(CustomerCreated event) {
        log.info("Customer got created with ID : {}", event.getPayload());

        markDone(event);
    }
    
}
