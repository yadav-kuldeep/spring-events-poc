package com.noosyn.springevents.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EventMapper {
    private static ObjectMapper mapper = new ObjectMapper()
                    .registerModule(new JavaTimeModule());

    public static Event mapToObj(String json, String type) {
        try {
            Class<?> clazz = Class.forName(type);

            return (Event) mapper.readValue(json, clazz);
        } catch (ClassNotFoundException | JsonProcessingException e) {
            log.error("Could not parse json to type", e);
            throw new IllegalArgumentException(e);        
        }
    }
    
}
