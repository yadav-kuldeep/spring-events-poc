package com.noosyn.springevents.core;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public abstract class Event<T> {
    private final String id;
    private LocalDateTime occurredOn;
    private T payload;

    public Event(String id, LocalDateTime occurredOn, T payload) {
        this.id = id;
        this.occurredOn = occurredOn;
        this.payload = payload;
    }
}
