package com.noosyn.springevents.core;

import static org.springframework.transaction.event.TransactionPhase.AFTER_ROLLBACK;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.noosyn.springevents.EventLogService;
import com.noosyn.springevents.models.EventLog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractEventListener {
    @Autowired
    private EventLogService service;

    @Autowired
    private ObjectMapper mapper;

    @Order(value = Ordered.HIGHEST_PRECEDENCE)
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void handleEventBeforeCompletion(Event<?> event) {
        log.info("Received event {} before commit", event);
        try{
            String data = mapper.writeValueAsString(event);

            EventLog eventLog = EventLog.of(event.getId(), event.getClass().getName(), event.getOccurredOn(), data);

            service.upsert(eventLog);
        } catch(JsonProcessingException ex) {
            log.error("Unable to convert event to event log", ex);
            throw new IllegalArgumentException("Unable to convert event to event log");
        }
    }
    
    @TransactionalEventListener(phase = AFTER_ROLLBACK)
    public void handleEventAfterRollback(Event<?> event) {
        log.info("Received event {} after rollback", event);
    }

    public void markDone(Event<?> event) {
        service.markDone(event.getId());
        
    }
}
