package com.noosyn.springevents;

import java.time.LocalDateTime;
import java.util.UUID;

import com.noosyn.springevents.events.CustomerCreated;
import com.noosyn.springevents.models.Customer;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
@Transactional
public class CustomerService {
    private final ApplicationEventPublisher publisher;
    private final CustomerRepository repository;

    public Customer upsert(Customer customer) {
        try{
            Customer aCustomer = repository.save(customer);
            publisher.publishEvent(
                new CustomerCreated(
                    UUID.randomUUID().toString(), 
                    LocalDateTime.now(),
                    aCustomer.getId()
                )
            );
            return aCustomer;
        }finally {
            // throw new IllegalStateException("Need to check what happens when error occurs before commit");
        }
    }
    
}
