package com.noosyn.springevents.events;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.noosyn.springevents.core.Event;

public class CustomerCreated extends Event<Integer> {
    @JsonCreator
    public CustomerCreated(@JsonProperty("id") String id,
                           @JsonProperty("occurredOn") LocalDateTime occurredOn,
                           @JsonProperty("payload") Integer customerId) {
        super(id, occurredOn, customerId);
    }
    
}
