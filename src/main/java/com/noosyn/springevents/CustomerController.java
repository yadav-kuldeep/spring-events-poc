package com.noosyn.springevents;

import java.net.URI;

import com.noosyn.springevents.models.Customer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService service;

    @PostMapping("/customers")
    public ResponseEntity<Void> createCustomer(@RequestBody Customer customer) {
        Customer aCustomer = service.upsert(customer);
        return ResponseEntity.created(URI.create("/customers/"+aCustomer.getId())).build();
    }
    
}
